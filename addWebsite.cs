﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Sipag
{
    public partial class addWebsite : Form
    {
        public string urlAddress;
        public bool isAllowed;
        public addWebsite()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void addURL_Click(object sender, EventArgs e)
        {
            

            //Regex validation of URL here
            string valid_url = @"[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}";
            Regex regex = new Regex(valid_url);

            bool matches = regex.IsMatch(url.Text);
            if (matches)
            {
                regexResult.Text = "Everything looks good!";
                regexResult.ForeColor = Color.Green;
                urlAddress = url.Text;
                isAllowed = isAllowedChecked.Checked;
                this.Close();
            }
            else
            {
                regexResult.Text = "Something doesn't look right here.";
                regexResult.ForeColor = Color.Red;
                url.Focus();
                url.SelectAll();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
