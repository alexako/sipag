﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace Sipag
{
    public partial class Sipag : Form
    {
        public static int hours, minutes, seconds = 0;
        public static bool showTimer = false, initialized = true;
        public static string timeRemaining,
            path = @"C:\Users\alex\Documents\Sipag", //Path to list and schedule
            allow_block_file = "allow_block_list.spg", //List filename
            schedule_file = "schedule.spg", //Schedule filename
            hosts_path = @"C:\Windows\System32\Drivers\etc\",
            hosts = "hosts",
            backup_file = hosts_path + hosts + ".backup";
        public Dictionary<string, List<string>> allow_block_Dict = new Dictionary<string, List<string>>();
        public TimerDisplay display = new TimerDisplay();
        public NewTimer setTimer = new NewTimer();
        public Schedule makeSchedule = new Schedule();
        static Timer timer = new Timer();
        Splash spl; //Splash Page


        public Sipag()
        {
            InitializeComponent();

            //Splash and progressbar
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            spl = new Splash();
        }

        private void Sipag_Load(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy != true)
            {
                spl.Show(); //Show splash screen
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int progress = 0;
            //TODO:
            //Setup where to save *.spg files
            //Default filename: allow_block_list.spg
            //(Default path: @"C:\Users\alex\Documents\Sipag\")

            DirectoryInfo dir;
            if (!(Directory.Exists(path)))
            {
                try
                {
                    dir = Directory.CreateDirectory(path);
                    initialized = false;
                }
                catch { }
            }

            worker.ReportProgress(progress++);
            System.Threading.Thread.Sleep(300);

            //Back up hosts file
            if (!(File.Exists(Path.Combine(path, backup_file))) && !initialized)
            {
                try
                {
                    File.Copy(Path.Combine(hosts_path, hosts), Path.Combine(path, backup_file), true);
                    File.Copy(Path.Combine(hosts_path, hosts), Path.Combine(hosts_path, backup_file), true);
                    initialized = true;
                }
                catch { }
            }
            else
                File.Copy(Path.Combine(hosts_path, hosts), Path.Combine(hosts_path, backup_file), true);

            //Load list file to dictionary
            if (File.Exists(Path.Combine(path, allow_block_file)))
            {
                string line, status, url;
                double lineCount = File.ReadLines(Path.Combine(path, allow_block_file)).Count();
                StreamReader file = new StreamReader(Path.Combine(path, allow_block_file));
                while ((line = file.ReadLine()) != null)
                {
                    status = line.Split()[0];
                    url = line.Split()[1];
                    List<string> data = new List<string>();
                    try
                    {
                        data.Add(status);
                        data.Add("127.0.0.1");
                        allow_block_Dict.Add(url, data);
                    }
                    catch { }
                    if (progress > 100)
                        worker.ReportProgress(progress += (Convert.ToInt32(100 / lineCount)));
                }
                file.Close();

                worker.ReportProgress(100);
                System.Threading.Thread.Sleep(400);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            spl.progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            spl.Close();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Start_Timer();
        }

        private void newTimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Start_Timer();
        }

        private void newScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {   
            makeSchedule.ShowDialog();
        }

        private void editAllowBlockListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllowBlockList allow_block = new AllowBlockList();
            allow_block.ShowDialog();
        }

        //TODO:
        //FUNCTION Check dateTime is in schedule 
        //Parameters (scheduleFile, reminderEnabled)
        //(Keep running in background and possibly at startup)
        //If (reminderEnabled && DateTime.Now is <= 15 minutes of schedule)
        //  Display reminder dialog

        public static bool CheckForInternetConnection()
        {
            //This function current not being used anywhere (DELETE ME)
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets public IP addresses for given hostname (No longer needed)
        /// </summary>
        /// <param name="hostname">ex: example.com</param>
        /// <returns>List of ip addresses</returns>
        public static List<string> GetHostAddress(string hostname)
        {
            //This is not neeeded. Must use localhost instead (DELETE ME)
            //Could possible use IP address of custom block notif or something

            //Get all IP addresses for hostname
            //Returns List<string> 
            
            List<string> ip_addresses = new List<string>();

            try
            {
                IPAddress[] ips = Dns.GetHostAddresses(hostname);
                foreach (var ip in ips)
                    ip_addresses.Add(ip.ToString());
            }
            catch (ArgumentNullException e)
            {
                string x = "ArgumentNullException caught!" + "\n" +
                    "website : " + hostname + "\n" +
                    "Source : " + e.Source + "\n" +
                    "Message : " + e.Message;
                MessageBox.Show(x, "Error");
            }
            catch (Exception e)
            {
                string x = "Exception caught!" + "\n" +
                    "URL : " + hostname + "\n" +
                    "Source : " + e.Source + "\n" +
                    "Message : " + e.Message;
                MessageBox.Show(x, "Error");
            }
            finally
            {
                if (ip_addresses.Count < 1)
                    ip_addresses.Add("Failed to fetch...");
            }
            
            return ip_addresses;
        }

        private void Update_Text(object sender, EventArgs e)
        {
            if (minutes == 0 && hours == 0 && seconds == 0)
            {
                //Clear all fields/timer
                timer.Enabled = false;
                timer.Stop();
                if (showTimer) display.Close();
                Deactivate_Restrictions();
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                if (seconds < 1)
                {
                    seconds = 59;
                    if (minutes == 0)
                    {
                        minutes = 59;
                        if (hours != 0) hours -= 1;
                    }
                    else minutes -= 1;
                }
                else seconds -= 1;

                //Format values to 2 digits
                timeRemaining = hours.ToString("00") + ":" +
                    minutes.ToString("00") + ":" +
                    seconds.ToString("00");
                display.TimeRemaining = timeRemaining;
            }
        }

        private void Start_Timer()
        {
            DialogResult result = setTimer.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.WindowState = FormWindowState.Minimized;

                //Set up timer
                timer.Tick += new EventHandler(Update_Text);
                timer.Interval = 1000;
                timer.Enabled = true;

                Activate_Restrictions();

                timer.Start();
                if (showTimer)
                    display.ShowDialog();
            }
            else
                this.WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// Activates website restrictions by editing the hosts file
        /// </summary>
        /// <param name="allowBlockList"></param>
        private void Block_Websites(Dictionary<string, List<string>> allowBlockList)
        {
            //Back up hosts file
            File.Copy(Path.Combine(hosts_path, hosts), Path.Combine(hosts_path, backup_file), true);

            List<string> data = new List<string>();
            string status, url;
            foreach (var item in allowBlockList)
            {
                data = item.Value;
                status = data[0];
                url = item.Key;
                if (status == "Blocked")
                {
                    foreach (var ip in data.GetRange(1, data.Count-1))
                    {
                        //Append blocked sites to hosts file
                        using (StreamWriter sw = File.AppendText(Path.Combine(hosts_path, hosts)))
                        {
                            sw.WriteLine("127.0.0.1" + " " + "www." + url);
                        }
                    }
                }
            }
        }

        private void Activate_Restrictions()
        {
            Block_Websites(allow_block_Dict);
            //TODO: Block access to applications
        }

        /// <summary>
        /// Deactivate restrictions by reverting hosts file to original state before program was launched and...
        /// </summary>
        public static void Deactivate_Restrictions()
        {
            //Lift website restrictions
            try
            {
                File.Copy(Path.Combine(hosts_path, backup_file), Path.Combine(path, hosts), true);
                //TODO: Lift application restrictions
            }
            catch { }
        }

        //public static string Redirect_Websites(HttpListenerRequest request)
        //{
        //    return string.Format("<HTML><BODY>My web page.<br>{0}</BODY></HTML>", DateTime.Now);
        //}

        private void Sipag_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Deactivate_Restrictions();
                MessageBox.Show("Restrictions lifted successfully."); //DELETE ME
            }
            catch { }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Deactivate_Restrictions();
                Application.Exit();
            }
            catch { }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Display About page
            About about = new About();
            about.ShowDialog();
        }

        private void resetToDefaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to refresh to default settings?", "", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                //Build default lists
                string[] defaultWebsites = 
                        {
                                "reddit.com",
                                "facebook.com",
                                "amazon.com",
                                "ebay.com",
                                "xkcd.com"
                        };
                Dictionary<string, string> defaultweblist = new Dictionary<string, string>();
                foreach (var website in defaultWebsites)
                    defaultweblist.Add(website, "Blocked");

                List<string> saveList = new List<string>();
                foreach (var item in defaultweblist)
                    saveList.Add(item.Value + " " + item.Key);

                try
                {
                    //Save default list
                    File.WriteAllLines(Path.Combine(path, allow_block_file), saveList);

                    //Restore hosts file to original settings
                    File.Copy(Path.Combine(path, backup_file), Path.Combine(hosts_path, hosts), true);

                    MessageBox.Show("Files have been refreshed.");
                }
                catch (Exception er)
                {
                    MessageBox.Show("Error: Could not save file.\nReason: " + er.Message);
                }
            }
            else
                MessageBox.Show("Refresh aborted.");
        }
    }
}
