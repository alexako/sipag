﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//to keep window on top
using System.Runtime.InteropServices;

namespace Sipag
{

    public partial class TimerDisplay : Form
    {
        
        //Keep display on top
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        //END display on top
        

        public string TimeRemaining
        {
            get
            {
                return this.timeRemaining.Text;
            }
            set
            {
                this.timeRemaining.Text = value;
            }
        }

        public TimerDisplay()
        {
            InitializeComponent();
        }

        private void TimerDisplay_Load(object sender, EventArgs e)
        {
            SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void TimerDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Sipag.Deactivate_Restrictions();
                MessageBox.Show("Restrictions lifted successfully.");
            }
            catch { }
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to stop the timer?", "", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Sipag.hours = 0;
                Sipag.minutes = 0;
                Sipag.seconds = 0;
            }
        }
    }
}
