﻿namespace Sipag
{
    partial class AllowBlockList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AllowBlockList));
            this.websites = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.allow = new System.Windows.Forms.Button();
            this.block = new System.Windows.Forms.Button();
            this.deselectAll = new System.Windows.Forms.Button();
            this.selectAll = new System.Windows.Forms.Button();
            this.addWebsite = new System.Windows.Forms.Button();
            this.removeWebsite = new System.Windows.Forms.Button();
            this.websitesList = new System.Windows.Forms.ListView();
            this.status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.webAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ipAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.websites.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // websites
            // 
            this.websites.Controls.Add(this.tabPage1);
            this.websites.Controls.Add(this.tabPage2);
            this.websites.Location = new System.Drawing.Point(12, 27);
            this.websites.Name = "websites";
            this.websites.SelectedIndex = 0;
            this.websites.Size = new System.Drawing.Size(385, 412);
            this.websites.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.allow);
            this.tabPage1.Controls.Add(this.block);
            this.tabPage1.Controls.Add(this.deselectAll);
            this.tabPage1.Controls.Add(this.selectAll);
            this.tabPage1.Controls.Add(this.addWebsite);
            this.tabPage1.Controls.Add(this.removeWebsite);
            this.tabPage1.Controls.Add(this.websitesList);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(377, 386);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Websites";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // allow
            // 
            this.allow.Location = new System.Drawing.Point(297, 171);
            this.allow.Name = "allow";
            this.allow.Size = new System.Drawing.Size(75, 23);
            this.allow.TabIndex = 6;
            this.allow.Text = "Allow";
            this.allow.UseVisualStyleBackColor = true;
            this.allow.Click += new System.EventHandler(this.allow_Click);
            // 
            // block
            // 
            this.block.Location = new System.Drawing.Point(296, 200);
            this.block.Name = "block";
            this.block.Size = new System.Drawing.Size(75, 23);
            this.block.TabIndex = 5;
            this.block.Text = "Block";
            this.block.UseVisualStyleBackColor = true;
            this.block.Click += new System.EventHandler(this.block_Click);
            // 
            // deselectAll
            // 
            this.deselectAll.Location = new System.Drawing.Point(296, 97);
            this.deselectAll.Name = "deselectAll";
            this.deselectAll.Size = new System.Drawing.Size(75, 23);
            this.deselectAll.TabIndex = 4;
            this.deselectAll.Text = "Deselect All";
            this.deselectAll.UseVisualStyleBackColor = true;
            this.deselectAll.Click += new System.EventHandler(this.deselectAll_Click);
            // 
            // selectAll
            // 
            this.selectAll.Location = new System.Drawing.Point(296, 68);
            this.selectAll.Name = "selectAll";
            this.selectAll.Size = new System.Drawing.Size(75, 23);
            this.selectAll.TabIndex = 3;
            this.selectAll.Text = "Select All";
            this.selectAll.UseVisualStyleBackColor = true;
            this.selectAll.Click += new System.EventHandler(this.button1_Click);
            // 
            // addWebsite
            // 
            this.addWebsite.Location = new System.Drawing.Point(235, 357);
            this.addWebsite.Name = "addWebsite";
            this.addWebsite.Size = new System.Drawing.Size(29, 23);
            this.addWebsite.TabIndex = 2;
            this.addWebsite.Text = "+";
            this.addWebsite.UseVisualStyleBackColor = true;
            this.addWebsite.Click += new System.EventHandler(this.addWebsite_Click);
            // 
            // removeWebsite
            // 
            this.removeWebsite.Location = new System.Drawing.Point(261, 357);
            this.removeWebsite.Name = "removeWebsite";
            this.removeWebsite.Size = new System.Drawing.Size(29, 23);
            this.removeWebsite.TabIndex = 1;
            this.removeWebsite.Text = "-";
            this.removeWebsite.UseVisualStyleBackColor = true;
            this.removeWebsite.Click += new System.EventHandler(this.removeWebsite_Click);
            // 
            // websitesList
            // 
            this.websitesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.status,
            this.webAddress,
            this.ipAddress});
            this.websitesList.FullRowSelect = true;
            this.websitesList.HideSelection = false;
            this.websitesList.Location = new System.Drawing.Point(7, 7);
            this.websitesList.Name = "websitesList";
            this.websitesList.Size = new System.Drawing.Size(283, 344);
            this.websitesList.TabIndex = 0;
            this.websitesList.UseCompatibleStateImageBehavior = false;
            this.websitesList.View = System.Windows.Forms.View.Details;
            // 
            // status
            // 
            this.status.Text = "Status";
            this.status.Width = 80;
            // 
            // webAddress
            // 
            this.webAddress.Text = "Website URL";
            this.webAddress.Width = 117;
            // 
            // ipAddress
            // 
            this.ipAddress.Text = "IP Address";
            this.ipAddress.Width = 82;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(377, 386);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Applications";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(409, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.saveToolStripMenuItem.Text = "&Save...";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.openToolStripMenuItem.Text = "&Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.closeToolStripMenuItem.Text = "&Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Coming soon...";
            // 
            // AllowBlockList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 451);
            this.ControlBox = false;
            this.Controls.Add(this.websites);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AllowBlockList";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AllowBlockList";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AllowBlockList_FormClosing);
            this.Load += new System.EventHandler(this.AllowBlockList_Load);
            this.websites.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl websites;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button addWebsite;
        private System.Windows.Forms.Button removeWebsite;
        private System.Windows.Forms.ListView websitesList;
        private System.Windows.Forms.ColumnHeader status;
        private System.Windows.Forms.ColumnHeader webAddress;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button selectAll;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.Button deselectAll;
        private System.Windows.Forms.Button allow;
        private System.Windows.Forms.Button block;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader ipAddress;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label1;
    }
}