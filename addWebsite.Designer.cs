﻿namespace Sipag
{
    partial class addWebsite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.url = new System.Windows.Forms.TextBox();
            this.addURL = new System.Windows.Forms.Button();
            this.isAllowedChecked = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.regexResult = new System.Windows.Forms.Label();
            this.cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // url
            // 
            this.url.Location = new System.Drawing.Point(45, 28);
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(168, 20);
            this.url.TabIndex = 0;
            this.url.Text = "example.com";
            // 
            // addURL
            // 
            this.addURL.Location = new System.Drawing.Point(219, 28);
            this.addURL.Name = "addURL";
            this.addURL.Size = new System.Drawing.Size(75, 20);
            this.addURL.TabIndex = 1;
            this.addURL.Text = "Add";
            this.addURL.UseVisualStyleBackColor = true;
            this.addURL.Click += new System.EventHandler(this.addURL_Click);
            // 
            // isAllowedChecked
            // 
            this.isAllowedChecked.AutoSize = true;
            this.isAllowedChecked.Location = new System.Drawing.Point(45, 54);
            this.isAllowedChecked.Name = "isAllowedChecked";
            this.isAllowedChecked.Size = new System.Drawing.Size(158, 17);
            this.isAllowedChecked.TabIndex = 2;
            this.isAllowedChecked.Text = "Allow access to this website";
            this.isAllowedChecked.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "http://";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // regexResult
            // 
            this.regexResult.AutoSize = true;
            this.regexResult.Location = new System.Drawing.Point(45, 9);
            this.regexResult.Name = "regexResult";
            this.regexResult.Size = new System.Drawing.Size(0, 13);
            this.regexResult.TabIndex = 4;
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(219, 48);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 5;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // addWebsite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 89);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.regexResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.isAllowedChecked);
            this.Controls.Add(this.addURL);
            this.Controls.Add(this.url);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "addWebsite";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add A URL";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox url;
        private System.Windows.Forms.Button addURL;
        private System.Windows.Forms.CheckBox isAllowedChecked;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label regexResult;
        private System.Windows.Forms.Button cancel;
    }
}