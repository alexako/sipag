#Sipag
Sipag is a time management app.

###Features
- Set timer to block access to distractions such as websites or video games
- Create a schedule to keep you on track
- Remind yourself to get back to work after a break
