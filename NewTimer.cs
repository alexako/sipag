﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sipag
{
    public partial class NewTimer : Form
    {
        public NewTimer()
        {
            InitializeComponent();
        }

        private void NewTimer_Load(object sender, EventArgs e)
        {

        }

        private void start_Click(object sender, EventArgs e)
        {
            Sipag.hours = Convert.ToInt32(hours.Value);
            Sipag.minutes = Convert.ToInt32(minutes.Value);
            Sipag.showTimer = showTimer.Checked;
            this.Close();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void alwaysOnTop_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void timerOnTop_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void showTimer_CheckedChanged(object sender, EventArgs e)
        {
        }


    }
}
