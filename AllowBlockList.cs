﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Sipag
{
    public partial class AllowBlockList : Form
    {
        public string url, pathToFile = Path.Combine(Sipag.path, Sipag.allow_block_file);
        public bool changesDetected = false;
        Splash2 spl;

        public AllowBlockList()
        {
            InitializeComponent();

            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            spl = new Splash2();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            System.Threading.Thread.Sleep(1000);
            worker.ReportProgress(100);
        }

        private void AllowBlockList_Load(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy != true)
            {
                spl.Show();

                //Load allow_block list for ListView
                if (File.Exists(pathToFile))
                {
                    Load_List(websitesList, pathToFile);
                }
                else //Build default list of websites if no save file found
                {
                    string[] defaultWebsites = 
                    {
                        "reddit.com",
                        "facebook.com",
                        "amazon.com",
                        "ebay.com",
                        "xkcd.com"
                    };

                    foreach (var website in defaultWebsites)
                    {
                        string ip = "Unresolved";
                        try
                        {
                            if (Sipag.CheckForInternetConnection())
                                ip = Sipag.GetHostAddress(website)[0];
                        }
                        catch { }
                        string[] temp = { "Blocked", website, ip };
                        var listViewItem = new ListViewItem(temp);
                        websitesList.Items.Add(listViewItem);
                    }
                }
                //END ListView load

                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync();
            }

 
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            spl.Close();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        //Select all
        private void button1_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in websitesList.Items)
                item.Selected = true;
        }

        private void deselectAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in websitesList.Items)
                item.Selected = false;
        }

        private void allow_Click(object sender, EventArgs e)
        {
            changesDetected = true;
            foreach (ListViewItem item in websitesList.Items)
                item.Text = item.Selected ? "Allowed" : item.Text;
        }

        private void block_Click(object sender, EventArgs e)
        {
            changesDetected = true;
            foreach (ListViewItem item in websitesList.Items)
                item.Text = item.Selected ? "Blocked" : item.Text;
        }

        private void addWebsite_Click(object sender, EventArgs e)
        {
            changesDetected = true;
            addWebsite addURL = new addWebsite();
            addURL.ShowDialog();
            
            if (addURL.urlAddress != null)
            {
                string[] website = { addURL.isAllowed ? "Allowed" : "Blocked", addURL.urlAddress };
                var listViewItem = new ListViewItem(website);
                websitesList.Items.Add(listViewItem);
            }
        }

        private void removeWebsite_Click(object sender, EventArgs e)
        {
            changesDetected = true;
            foreach (ListViewItem item in websitesList.SelectedItems)
                websitesList.Items.Remove(item);
        }

        /// <summary>
        /// Loads the ListView with given file
        /// </summary>
        /// <param name="list"></param>
        /// <param name="path"></param>
        private void Load_List(ListView list, string path)
        {
            string line, status, url, ip = "Unresolved";
            StreamReader file = new StreamReader(path);
            list.Items.Clear(); //To avoid duplicates, but overwrites existing
            while ((line = file.ReadLine()) != null)
            {
                status = line.Split()[0];
                url = line.Split()[1];
                if (Sipag.CheckForInternetConnection())
                    ip = Sipag.GetHostAddress(url)[0];
                string[] item = { status, url, ip };
                //Check if item is not already in list
                if (!(list.Items.ContainsKey(url)))
                {
                    var listViewItem = new ListViewItem(item);
                    list.Items.Add(listViewItem);
                }
            }

            file.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save_AllowBlockList();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Filter = "Sipag files (*.spg)|*.spg|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.RestoreDirectory = true;
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Load_List(websitesList, openFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. \nReason: " + ex.Message);
                }
            }
        }

        private void Save_AllowBlockList()
        {
            List<string> saveList = new List<string>();
            foreach (ListViewItem item in websitesList.Items)
                saveList.Add(item.SubItems[0].Text + " " + item.SubItems[1].Text);

            try
            {
                File.WriteAllLines(pathToFile, saveList);
                MessageBox.Show("Saved successfully!");
                changesDetected = false;
            }
            catch (Exception er)
            {
                MessageBox.Show("Error: Could not save file.\nReason: " + er.Message);
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (changesDetected)
            {
                DialogResult result = MessageBox.Show("Changes were made. Would you like to save them?",
                    "Hang on!",
                    MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                {
                    Save_AllowBlockList();
                    this.Close();
                }
                else if (result == DialogResult.No)
                {
                    changesDetected = false;
                    this.Close();
                }
            }
            else
                this.Close();
        }

        private void AllowBlockList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (changesDetected)
            {
                DialogResult result = MessageBox.Show("Changes were made. Would you like to save them?",
                    "Hang on!",
                    MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                {
                    Save_AllowBlockList();
                    this.Close();
                }
                if (result == DialogResult.No)
                    this.Close();
            }
            else
                this.Close();
        }
    }
}
