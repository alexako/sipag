﻿namespace Sipag
{
    partial class NewTimer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hours = new System.Windows.Forms.NumericUpDown();
            this.colonTimer = new System.Windows.Forms.Label();
            this.minutes = new System.Windows.Forms.NumericUpDown();
            this.start = new System.Windows.Forms.Button();
            this.hoursLabel = new System.Windows.Forms.Label();
            this.minutesLabel = new System.Windows.Forms.Label();
            this.cancel = new System.Windows.Forms.Button();
            this.showTimer = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.hours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minutes)).BeginInit();
            this.SuspendLayout();
            // 
            // hours
            // 
            this.hours.Location = new System.Drawing.Point(80, 27);
            this.hours.Name = "hours";
            this.hours.Size = new System.Drawing.Size(42, 20);
            this.hours.TabIndex = 0;
            this.hours.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // colonTimer
            // 
            this.colonTimer.AutoSize = true;
            this.colonTimer.Location = new System.Drawing.Point(128, 29);
            this.colonTimer.Name = "colonTimer";
            this.colonTimer.Size = new System.Drawing.Size(10, 13);
            this.colonTimer.TabIndex = 1;
            this.colonTimer.Text = ":";
            // 
            // minutes
            // 
            this.minutes.Location = new System.Drawing.Point(144, 27);
            this.minutes.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.minutes.Name = "minutes";
            this.minutes.Size = new System.Drawing.Size(49, 20);
            this.minutes.TabIndex = 2;
            this.minutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.minutes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // start
            // 
            this.start.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.start.Location = new System.Drawing.Point(63, 79);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 4;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // hoursLabel
            // 
            this.hoursLabel.AutoSize = true;
            this.hoursLabel.Location = new System.Drawing.Point(77, 11);
            this.hoursLabel.Name = "hoursLabel";
            this.hoursLabel.Size = new System.Drawing.Size(35, 13);
            this.hoursLabel.TabIndex = 5;
            this.hoursLabel.Text = "Hours";
            // 
            // minutesLabel
            // 
            this.minutesLabel.AutoSize = true;
            this.minutesLabel.Location = new System.Drawing.Point(144, 11);
            this.minutesLabel.Name = "minutesLabel";
            this.minutesLabel.Size = new System.Drawing.Size(44, 13);
            this.minutesLabel.TabIndex = 6;
            this.minutesLabel.Text = "Minutes";
            // 
            // cancel
            // 
            this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancel.Location = new System.Drawing.Point(142, 79);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 7;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // showTimer
            // 
            this.showTimer.AutoSize = true;
            this.showTimer.Checked = true;
            this.showTimer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showTimer.Location = new System.Drawing.Point(80, 56);
            this.showTimer.Name = "showTimer";
            this.showTimer.Size = new System.Drawing.Size(119, 17);
            this.showTimer.TabIndex = 8;
            this.showTimer.Text = "Show Timer Display";
            this.showTimer.UseVisualStyleBackColor = true;
            this.showTimer.CheckedChanged += new System.EventHandler(this.showTimer_CheckedChanged);
            // 
            // NewTimer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 114);
            this.Controls.Add(this.showTimer);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.minutesLabel);
            this.Controls.Add(this.hoursLabel);
            this.Controls.Add(this.start);
            this.Controls.Add(this.minutes);
            this.Controls.Add(this.colonTimer);
            this.Controls.Add(this.hours);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewTimer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Create New Timer";
            this.Load += new System.EventHandler(this.NewTimer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.hours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minutes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown hours;
        private System.Windows.Forms.Label colonTimer;
        private System.Windows.Forms.NumericUpDown minutes;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label hoursLabel;
        private System.Windows.Forms.Label minutesLabel;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.CheckBox showTimer;
    }
}